package com.example.mastmymind;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.os.Handler;
import android.media.AudioManager;
import android.media.MediaPlayer.OnPreparedListener;
import android.util.Log;
import android.os.Message;


public class MainActivity extends Activity {
	
	Button juego;
	Button ayuda;
	MediaPlayer mp=null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        try  
        {  
             final MediaPlayer.OnPreparedListener onPreparedListener = new MediaPlayer.OnPreparedListener()   
             {  
                   
                  public void onPrepared(MediaPlayer mp)   
                  {  
                       mp.start(); 
                       mp.setOnCompletionListener(new OnCompletionListener() {
                           public void onCompletion(MediaPlayer mp) {
                           	
                               mp.start();
                               
                           	
                           }
                       });
                  }  
             };  
             final MediaPlayer.OnErrorListener onErrorListener = new MediaPlayer.OnErrorListener()   
             {  
                   
                  public boolean onError(MediaPlayer mp, int what, int extra)   
                  {  
                       Log.e(getPackageName(), String.format("Error(%s%s)", what, extra));  
                       return false;  
                  }  
             };  
             final Handler onPreparedHandle = new Handler()  
             {  
                  @Override  
                  public void handleMessage(Message msg)   
                  {  
                       mp.setOnErrorListener(onErrorListener); // set onErrorListener  
                       mp.setAudioStreamType(AudioManager.STREAM_MUSIC);  
                       onPreparedListener.onPrepared(mp);  
                  }  
             };  
             new Thread()  
             {  
                  @Override  
                  public void run()  
                  {  
                       mp = MediaPlayer.create(MainActivity.this, R.raw.backgroundsong); // create new instance of MediaPlayer and prepare it  
                       onPreparedHandle.sendMessage(onPreparedHandle.obtainMessage()); // send message to main thread  
                  }  
             }.start();  
        }  
        catch (Exception e)  
        {  
             Log.e(getPackageName(), e.toString());  
        }  
   
        
        juego = (Button) findViewById(R.id.newgame);
        ayuda = (Button) findViewById(R.id.newhelp);
        
    }
    
    public void newgame(View v){
    
    	Intent i = new Intent(getApplicationContext(), mindgame.class);
    	MainActivity.this.startActivity(i);
    
    	
    }
    
    public void help(View v){
        
    	Intent i = new Intent(getApplicationContext(), help.class);
    	startActivity(i);
    	
    }
    
   
    @Override
    protected void onPause() {
    	// TODO Auto-generated method stub
    	super.onPause();
    	mp.stop();
    }

    /*@Override
   protected void onResume() {
    	// TODO Auto-generated method stub
    	super.onResume();
    	mp.start();
    }*/
    
    @Override
    protected void onDestroy() {
    	// TODO Auto-generated method stub
    	super.onDestroy();
    	mp.stop();
    }
    
}