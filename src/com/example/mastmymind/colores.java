package com.example.mastmymind;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.support.v4.app.NavUtils;

public class colores extends Activity {
	
	Button colores[] = new Button[8];
	
	@Override 
	protected void onCreate(Bundle savedInstanceState) { 
	super.onCreate(savedInstanceState);
	setContentView(R.layout.colordialog);
	
	
	
	}
	
	public void select(View v){
		Button option = (Button) v;
		Intent resultIntent = new Intent();
		if(option.equals(findViewById(R.id.brojo))){
			resultIntent.putExtra("color", "rojo");
		}
		
		else if(option.equals(findViewById(R.id.bazul))){
			resultIntent.putExtra("color", "azul");
		}
		
		else if(option.equals(findViewById(R.id.bverde))){
			resultIntent.putExtra("color", "verde");
		}
		
		else if(option.equals(findViewById(R.id.bamarillo))){
			resultIntent.putExtra("color", "amarillo");
		}
		
		else if(option.equals(findViewById(R.id.bvioleta))){
			resultIntent.putExtra("color", "violeta");
		}
		
		else if(option.equals(findViewById(R.id.baqua))){
			resultIntent.putExtra("color", "aqua");
		}
		
		else if(option.equals(findViewById(R.id.bgris))){
			resultIntent.putExtra("color", "gris");
		}
		
		else if(option.equals(findViewById(R.id.bnegro))){
			resultIntent.putExtra("color", "negro");
		}
		
		setResult(Activity.RESULT_OK, resultIntent);
		finish();
		
	}
	
	} 
