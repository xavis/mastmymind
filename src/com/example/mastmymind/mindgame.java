package com.example.mastmymind;


import android.R.integer;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.Toast;
import android.media.MediaPlayer.OnPreparedListener;
import android.widget.TableRow;

@SuppressLint({ "ParserError", "ParserError", "ParserError", "ParserError" })
public class mindgame extends Activity {

	TableLayout table;
	TableRow rows[] = new TableRow[15];
	Button buttons[]=new Button[5];
	int i=0;
	Button actual;
	Integer clave[] = new Integer[5]; 
	MediaPlayer mp1;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mindtable);

		mp1 = MediaPlayer.create(mindgame.this, R.raw.backgroundsong);
		mp1.setOnPreparedListener( new OnPreparedListener() {

			public void onPrepared(MediaPlayer mp1) {
				mp1.start();
			}
		}        );

		mp1.setOnCompletionListener(new OnCompletionListener() {
			public void onCompletion(MediaPlayer mp) {
				mp.start();
			}
		});

		for(int i=0;i<5;i++){

			clave[i]= (int)(Math.random()*8);

		}

		table=(TableLayout) findViewById(R.id.gameTable);
		addPlay();

		Log.d("RESPUESTAS", "SON: "+clave[0]+" "+clave[1]+" "+clave[2]+" "+clave[3]+" "+clave[4]+" ");



	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.gamemenu, menu);
		return true;
	}

	public void addPlay(){
		LayoutInflater inflater = getLayoutInflater();
		TableRow row = (TableRow)inflater.inflate(R.layout.gameplay, table, false);
		buttons[0]=(Button) row.findViewById(R.id.button1);
		buttons[1]=(Button) row.findViewById(R.id.button2);
		buttons[2]=(Button) row.findViewById(R.id.button3);
		buttons[3]=(Button) row.findViewById(R.id.button4);
		buttons[4]=(Button) row.findViewById(R.id.button5);
		row.setTag(i);

		table.addView(row);
		rows[i]=row;
		i++;
		MediaPlayer mp = MediaPlayer.create(mindgame.this, R.raw.gon);
		mp.setOnPreparedListener( new OnPreparedListener() {

			public void onPrepared(MediaPlayer mp) {
				mp.start();
			}
		}        );
	}

	@SuppressLint("ParserError")
	public void colorSelect(View v){
		Intent i = new Intent(this,colores.class);
		actual =  (Button) v;
		startActivityForResult(i, 800);
		mp1.start();

	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {     
		super.onActivityResult(requestCode, resultCode, data); 
		switch(requestCode) { 
		case (800) : { 
			if (resultCode == Activity.RESULT_OK) { 
				String newText = data.getStringExtra("color");

				if(newText.equals("rojo")){

					Drawable d = actual.getBackground();  
					PorterDuffColorFilter filter = new PorterDuffColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);  
					d.setColorFilter(filter);
					actual.setTag(0);

				}

				else if(newText.equals("azul")){

					Drawable d = actual.getBackground();  
					PorterDuffColorFilter filter = new PorterDuffColorFilter(Color.BLUE, PorterDuff.Mode.SRC_ATOP);  
					d.setColorFilter(filter);
					actual.setTag(1);

				}

				else if(newText.equals("verde")){

					Drawable d = actual.getBackground();  
					PorterDuffColorFilter filter = new PorterDuffColorFilter(Color.GREEN, PorterDuff.Mode.SRC_ATOP);  
					d.setColorFilter(filter);
					actual.setTag(2);

				}

				else if(newText.equals("amarillo")){

					Drawable d = actual.getBackground();  
					PorterDuffColorFilter filter = new PorterDuffColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);  
					d.setColorFilter(filter);
					actual.setTag(3);

				}

				else if(newText.equals("violeta")){
					Drawable d = actual.getBackground();  
					PorterDuffColorFilter filter = new PorterDuffColorFilter(Color.rgb(255, 0, 255), PorterDuff.Mode.SRC_ATOP);  
					d.setColorFilter(filter);
					actual.setTag(4);

				}

				else if(newText.equals("aqua")){

					Drawable d = actual.getBackground();  
					PorterDuffColorFilter filter = new PorterDuffColorFilter(Color.rgb(0, 255, 255), PorterDuff.Mode.SRC_ATOP);  
					d.setColorFilter(filter);
					actual.setTag(5);

				}

				else if(newText.equals("gris")){

					Drawable d = actual.getBackground();  
					PorterDuffColorFilter filter = new PorterDuffColorFilter(Color.LTGRAY, PorterDuff.Mode.SRC_ATOP);  
					d.setColorFilter(filter);
					actual.setTag(6);

				}

				else if(newText.equals("negro")){

					Drawable d = actual.getBackground();  
					PorterDuffColorFilter filter = new PorterDuffColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);  
					d.setColorFilter(filter);
					actual.setTag(7);

				}

			} 
			break; 
		} 
		} 
	}


	@Override
	public void onBackPressed() {

		final mindgame actividad = mindgame.this;

		final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		AlertDialog.Builder builder = new AlertDialog.Builder(actividad);
		builder.setTitle(getString(R.string.adv));
		builder.setMessage(getString(R.string.wannaexit));
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				actividad.finish();
			}});
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				alertDialog.dismiss();
			}});
		builder.show();

	}

	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.help:
			startActivity(new Intent(this, help.class));
			mp1.start();
			return true;
		case R.id.test:
			test();
			return true;
		default:
			return super.onOptionsItemSelected(item);

		}
	}

	public void test(){
		Integer bolas[] = new Integer[5];
		int bolascontador=0;
		int acertados=0;

		for(int i=0;i<5;i++){
			bolas[i]=0;
		}


		for(int i=0;i<5;i++){
			int k=0;
			for(int j=0;j<5;j++){

				if(buttons[i].getTag()==clave[j]&&i==j){
					if(k>0){
						bolascontador=bolascontador-k;
					}
					bolas[bolascontador]=1;

					bolascontador++;
					acertados++;

					break;

				}

				else if(buttons[i].getTag()==clave[j]){
					if(k>0){
						bolascontador=bolascontador-k;
						k=0;
					}
					k++;
					bolas[bolascontador]=2;
					bolascontador++;

				}

			}
		}

		Button botones[] = new Button[5];

		LayoutInflater inflater = getLayoutInflater();
		TableRow row = (TableRow)inflater.inflate(R.layout.gameanswer, table, false);
		botones[0]=(Button) row.findViewById(R.id.button1);
		botones[1]=(Button) row.findViewById(R.id.button2);
		botones[2]=(Button) row.findViewById(R.id.button3);
		botones[3]=(Button) row.findViewById(R.id.button4);
		botones[4]=(Button) row.findViewById(R.id.button5);

		for(int i=0;i<5;i++){
			if(bolas[i]==1){

				botones[i].setBackgroundResource(R.drawable.ballblack);

			}

			if(bolas[i]==2){

				botones[i].setBackgroundResource(R.drawable.ballwhite);

			}

			if(bolas[i]==0){
				botones[i].setBackgroundDrawable(null);
			}

			buttons[i].setEnabled(false);

		}

		table.addView(row);


		if(acertados==5){ 
			final mindgame actividad = mindgame.this;


			AlertDialog alertDialog = new AlertDialog.Builder(actividad).create();
			alertDialog.setTitle(getString(R.string.endgame));
			alertDialog.setMessage(getString(R.string.win));

			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					actividad.finish();
				}
			});
			alertDialog.setCancelable(false);
			alertDialog.show();  
			MediaPlayer mp = MediaPlayer.create(mindgame.this, R.raw.victory);
			mp.setOnPreparedListener( new OnPreparedListener() {

				public void onPrepared(MediaPlayer mp) {
					mp.start();
				}
			}        );

		}

		if(i>=15&&acertados!=5){
			final mindgame actividad = mindgame.this;
			AlertDialog alertDialog2 = new AlertDialog.Builder(this).create();
			alertDialog2.setTitle(getString(R.string.endgame));
			alertDialog2.setMessage(getString(R.string.lose));
			alertDialog2.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				actividad.finish();
				}
			});
			alertDialog2.setCancelable(false);
			alertDialog2.show();
			MediaPlayer mp = MediaPlayer.create(mindgame.this, R.raw.lose);
			mp.setOnPreparedListener( new OnPreparedListener() {

				public void onPrepared(MediaPlayer mp) {
					mp.start();
				}
			}        );
		}


		else if(i<15&&acertados!=5){
			addPlay();
			Integer gamecount = 15-i;
			Toast.makeText(getApplicationContext(), getString(R.string.remain)+" "+gamecount.toString()+" "+getString(R.string.plays), Toast.LENGTH_SHORT).show();
		}
	}



	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mp1.pause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mp1.start();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		mp1.stop();
	}




}